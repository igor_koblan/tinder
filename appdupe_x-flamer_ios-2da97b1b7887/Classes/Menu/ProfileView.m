//
//  ProfileView.m
//  Tinder
//
//  Created by CP0007 on 2/12/15.
//  Copyright (c) 2015 AppDupe. All rights reserved.
//

#import "ProfileView.h"
#import "RoundedImageView.h"

@interface ProfileView() {
    UIImageView *_backgroundView;
}

@property (nonatomic, strong) RoundedImageView *avatarView;
@property (nonatomic, strong) UILabel *titleLabel;

@end;

@implementation ProfileView
@synthesize titleLabel = _titleLabel;
#pragma mark -
#pragma mark - Public setters

- (void)setAvatarTitle:(NSString *)avatarTitle {
    self.titleLabel.text = avatarTitle;
}

- (void)setImage:(UIImage *)image {
    if (_backgroundView) {
        _backgroundView.image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
        _backgroundView.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundView.clipsToBounds = YES;
    }
}

#pragma mark -
#pragma mark - Private getters

- (RoundedImageView *)avatarView {
    if (_avatarView == nil) {
        _avatarView = [[RoundedImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 90, 90)];
        [self addSubview:_avatarView];
    }
    return _avatarView;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 170, 20)];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont boldSystemFontOfSize:17.0];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

#pragma mark -
#pragma mark - Init & layout

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _backgroundView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:_backgroundView];
        
        UILabel *lDefaultLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.avatarView.frame), self.bounds.size.height / 2 + 3.0, self.bounds.size.width - CGRectGetMaxX(self.avatarView.frame), 20.0)];
        lDefaultLabel.text = @"View profile";
        lDefaultLabel.textColor = [UIColor whiteColor];
        lDefaultLabel.textAlignment = NSTextAlignmentCenter;
        lDefaultLabel.backgroundColor = [UIColor clearColor];
        lDefaultLabel.font = [UIFont boldSystemFontOfSize:9.0];
        [self addSubview:lDefaultLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.avatarView.center = CGPointMake(self.avatarView.bounds.size.width / 2.0 + 15.0, self.bounds.size.height / 2.0);
    if (_backgroundView) {
        self.avatarView.image =[_backgroundView.image copy];
    }
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarView.frame), self.bounds.size.height / 2 - 15.0, self.bounds.size.width - CGRectGetMaxX(self.avatarView.frame) - 60.0, 20.0);
}

@end
