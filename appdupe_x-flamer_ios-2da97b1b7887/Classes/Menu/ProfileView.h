//
//  ProfileView.h
//  Tinder
//
//  Created by CP0007 on 2/12/15.
//  Copyright (c) 2015 AppDupe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileView : UIControl


@property (nonatomic, strong) NSString *avatarTitle;
@property (nonatomic, strong) UIImage *image;
@end
