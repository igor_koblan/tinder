//
//  UserMedia.m
//  Tinder
//
//  Created by Пользователь on 28.02.15.
//  Copyright (c) 2015 AppDupe. All rights reserved.
//

#import "UserMedia.h"


@implementation UserMedia

@dynamic media_id;
@dynamic url;
@dynamic tag;
@dynamic thumb_url;
@dynamic path;

@end
