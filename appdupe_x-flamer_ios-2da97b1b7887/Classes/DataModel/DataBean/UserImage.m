//
//  UserImage.m
//  Tinder
//
//  Created by Elluminati - macbook on 28/05/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//

#import "UserImage.h"

@implementation UserImage

@synthesize image_id,image_url,index_id, thumb_url;

- (NSString *)description {
    return [NSString stringWithFormat:@"image_id: %@, image_url: %@, index_id: %@, thumb_url: %@",image_id,image_url,index_id,thumb_url];
}
@end
