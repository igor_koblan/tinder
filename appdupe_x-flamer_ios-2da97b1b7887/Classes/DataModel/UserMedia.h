//
//  UserMedia.h
//  Tinder
//
//  Created by Пользователь on 28.02.15.
//  Copyright (c) 2015 AppDupe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserMedia : NSManagedObject

@property (nonatomic, retain) NSNumber * media_id;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * tag;
@property (nonatomic, retain) NSString * thumb_url;
@property (nonatomic, retain) NSString * path;

@end
