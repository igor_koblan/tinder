//
//  VideoView.m
//  Tinder
//
//  Created by Ihor Koblan on 2/18/15.
//  Copyright (c) 2015 AppDupe. All rights reserved.
//

#import "VideoButton.h"

@implementation VideoButton

@synthesize playerVC = _playerVC;

- (MPMoviePlayerController *)playerVC {
    if (!_playerVC) {
        _playerVC = [MPMoviePlayerController new];
        _playerVC.shouldAutoplay = NO;
        _playerVC.repeatMode = MPMovieRepeatModeNone;
//        _playerVC.movieSourceType = MPMovieSourceTypeFile;
        _playerVC.fullscreen = NO;
        
        [self addSubview:_playerVC.view];
    }
    return _playerVC;
}

- (instancetype)initWithURL1:(NSURL *)url {
    self = [super init];
    if (self) {
//        self.playerVC.contentURL = url;
//        [self.playerVC requestThumbnailImagesAtTimes:@[@1.0f] timeOption:MPMovieTimeOptionNearestKeyFrame];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.playerVC.contentURL = [NSURL URLWithString:@"http://vtinder.com/iOS/php/php/pics/profile_video1424816546000.mp4"];
    [self.playerVC play];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.userInteractionEnabled = YES;
       
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.playerVC.view.frame = self.bounds;
}


@end
