//
//  VideoView.h
//  Tinder
//
//  Created by Ihor Koblan on 2/18/15.
//  Copyright (c) 2015 AppDupe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface VideoButton : UIButton

@property (nonatomic, strong) MPMoviePlayerController *playerVC;

- (id)initWithURL1:(NSURL *)url;


@end