//
//  EditProfileVC.m
//  Tinder
//
//  Created by Elluminati - macbook on 14/05/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//

#import "EditProfileVC.h"

#import "UIImageView+Download.h"
#import "EditProfile.h"
#import "UserImage.h"
#import "Base64.h"
#import "AFHTTPRequestOperation.h"

@interface EditProfileVC ()
@property (nonatomic, strong) MPMoviePlayerController *theMoviPlayer;
@property (nonatomic, strong) NSMutableArray *arrImages;
@end

@implementation EditProfileVC

@synthesize strStatus;

- (NSMutableArray *)arrImages {
    _arrImages = [[DBHelper sharedObject] getObjectsforEntity:@"UserMedia"];
    return _arrImages;
}

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"Edit"];
    [self addrightButton:self.navigationItem];
    
    selectedBtnTag=-1;
    
    self.arrImages=[[NSMutableArray alloc]init];
    
    //UIImageView *img=(UIImageView *)[self.view viewWithTag:1000];
    //[img downloadFromURL:[User currentUser].profile_pic withPlaceholder:[UIImage imageNamed:@"pfImage.png"]];
    
    [self getUserPhotos];
    
    self.txtStatus.text=strStatus;
    

//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://vtinder.com/iOS/php/php/pics/profile_video1424814991000.mp4"]];
    self.theMoviPlayer = [MPMoviePlayerController new];
    self.theMoviPlayer.controlStyle = MPMovieControlStyleFullscreen;
    self.theMoviPlayer.movieSourceType = MPMovieSourceTypeStreaming;
    [self.theMoviPlayer setEndPlaybackTime:7.0];

//    [self.theMoviPlayer.view setFrame:CGRectMake(10, 10, 100, 100)];
//    [self.view addSubview:self.theMoviPlayer.view];
    

    
}

#pragma mark -
#pragma mark - NavBar Methods

-(void)addrightButton:(UINavigationItem*)naviItem
{
    //UIImage *imgButton = [UIImage imageNamed:@"chat_icon_off_line.png"];
	UIButton *rightbarbutton = [UIButton buttonWithType:UIButtonTypeCustom];
	[rightbarbutton setFrame:CGRectMake(0, 0, 75, 23)];
    [rightbarbutton setTitle:@"Done" forState:UIControlStateNormal];
    [rightbarbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightbarbutton setTitleShadowColor:[UIColor clearColor] forState:UIControlStateNormal];
    [[rightbarbutton titleLabel] setFont:[UIFont fontWithName:HELVETICALTSTD_LIGHT size:15.0]];
    
    [rightbarbutton addTarget:self action:@selector(doneEditing) forControlEvents:UIControlEventTouchUpInside];
    naviItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightbarbutton];
}

-(void)doneEditing
{
    [self dismissViewControllerAnimated:NO completion:nil];
    /*
    [UIView animateWithDuration:0.15
                          delay:0
                        options:UIViewAnimationOptionTransitionCurlUp
                     animations:^{
                         self.view.transform= CGAffineTransformMakeScale(1.3, 1.5);
                         
                         
                     }
                     completion:^(BOOL finished) {
                         self.view.transform = CGAffineTransformIdentity;
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];
     */
}

#pragma mark -
#pragma mark - Methods

-(void)getUserPhotos
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading..."];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:[User currentUser].fbid forKey:PARAM_ENT_USER_FBID];
    
    AFNHelper *afn=[[AFNHelper alloc] init];
    [afn getDataFromPath:METHOD_GET_USER_PROFILE_PIC withParamData:dictParam withBlock:^(id response, NSError *error) {
        if (response) {
            if ([[response objectForKey:@"errFlag"] intValue]==0) {

                [[DBHelper sharedObject] deleteObjectsForEntity:@"UserMedia"];
                NSArray *arr=[response objectForKey:@"Userphotos"];
                for (NSDictionary *dict in arr) {
                    
                    UserMedia *um = [[DBHelper sharedObject] createObjectForEntity:@"UserMedia"];
                    
                    um.media_id = @([[dict objectForKey:@"image_id"] integerValue]);
                    um.url = [dict objectForKey:@"image_url"];
                    um.tag = @([[dict objectForKey:@"index_id"] integerValue]);
                    
                    um.thumb_url = ([um.url hasSuffix:@"mp4"]) ? dict[@"thumb_url"] : nil;
                    [[DBHelper sharedObject] saveContext];
                }
                [self reloadImagesFromID:0 toID:6 withCallBack:^{
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                }];
            }
        }
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }];
}

-(void)reloadImagesFromID:(NSInteger)fromID toID:(NSInteger)toID withCallBack:(void (^)(void))callBack {
    NSLog(@"arrImgs: %@",self.arrImages);
    
    __block NSInteger blockCounter = self.arrImages.count;
    for (UserMedia *um in [self.arrImages copy]) {
        int tag = [um.tag intValue];
        UIImageView *img = (UIImageView *)[self.view viewWithTag:tag + 1000];

        UIButton *btn = (UIButton *)[self.view viewWithTag:tag + 2000];
        btn.selected=YES;
        img.contentMode =UIViewContentModeRedraw;
        [img downloadFromURL:(um.thumb_url ? um.thumb_url : um.url) withPlaceholder:[UIImage imageNamed:@"pfImage.png"] withSuccess:^{
            blockCounter --;
            if (callBack && (blockCounter == 0)) {
                callBack();
            }
        } failure:^(void) {
            blockCounter --;
            if (callBack && (blockCounter == 0)) {
                callBack();
            }
        }];
    }
    for (int i = 0; i < 6; i++) {

        NSArray *filtArr = [[DBHelper sharedObject] getObjectsforEntity:@"UserMedia" ShortBy:nil isAscending:YES predicate:[NSPredicate predicateWithFormat:@"tag == %i",i]];

        if (filtArr.count == 0) {
            UIImageView *imgView = (UIImageView *)[self.view viewWithTag:i + 1000];
            imgView.image = [UIImage imageNamed:@"pfImage.png"];
        }
    }
}

-(void)deleteImage:(void (^)(void))callBack {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Deleting..."];
    NSLog(@"count: %i",self.arrImages.count);
    UserMedia *ui=nil;
    for (int i=0; i<self.arrImages.count; i++) {
        UserMedia *um = [[DBHelper sharedObject] getObjectsforEntity:@"UserMedia" ShortBy:nil isAscending:YES predicate:[NSPredicate predicateWithFormat:@"tag == %i",selectedBtnTag]][0];
        if ([um.tag intValue]==selectedBtnTag) {
            ui = um;
        }
    }
    if (ui == nil) {
        return;
    }
    
    NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
    [dictParam setObject:[User currentUser].fbid forKey:PARAM_ENT_USER_FBID];
    [dictParam setObject:ui.media_id forKey:PARAM_ENT_IMAGE_ID];
    
    __weak typeof(self) weekSelf_ = self;
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromPath:METHOD_DELETE_USER_IMAGE withParamData:dictParam withBlock:^(id response, NSError *error) {
        if (response) {
            if ([[response objectForKey:@"errFlag"] intValue] == 0) {

                [[DBHelper sharedObject] deleteObject:ui];

                [weekSelf_ reloadImagesFromID:0 toID:6 withCallBack:^{
                    if (callBack) {
                        callBack();
                    }
                }];
            }
        }
    }];
}

#pragma mark -
#pragma mark - Actions

-(IBAction)onClickChangeStatus:(id)sender{
    [self.txtStatus resignFirstResponder];
    
    if (self.txtStatus.text.length==0) {
        return;
    }

    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading..."];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:[User currentUser].fbid forKey:PARAM_ENT_USER_FBID];
    [dictParam setObject:self.txtStatus.text forKey:PARAM_ENT_STATUS];
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromPath:METHOD_UPDATE_STATUS withParamData:dictParam withBlock:^(id response, NSError *error) {
        if (response) {
            if ([[response objectForKey:@"errFlag"] intValue]==0) {
                [[TinderAppDelegate sharedAppDelegate]showToastMessage:[response objectForKey:@"errMsg"]];
            }
        }
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }];
    
}

-(IBAction)onClickBtn:(id)sender {
    UIButton *btn=(UIButton *)sender;
    selectedBtnTag = btn.tag - 2000;
    
    if (btn.selected) {
        __weak typeof(btn) weekBtn_ = btn;
        [self deleteImage:^{
            weekBtn_.selected=NO;
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
        }];
    } else {
        if (selectedBtnTag < 3) {
            [self openCamera];
        } else {
            [self openVideoCamera];
        }
    }
}

-(IBAction)asd:(id)sender {
    
}

- (void)itemDidFinishPlaying:(id)obj {
    [self.theMoviPlayer stop];
    [self.theMoviPlayer.view removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

-(IBAction)onClickImage:(id)sender {
    [self.theMoviPlayer play];
    
    UIButton *btn = sender;
    int tag = btn.tag - 3000;

    NSArray *arr = [[DBHelper sharedObject] getObjectsforEntity:@"UserMedia" ShortBy:nil isAscending:YES predicate:[NSPredicate predicateWithFormat:@"tag == %i",tag]];
    if (arr.count == 0) {
        return;
    }
    
    if (tag < 3) {
        UIAlertView *alt=[[UIAlertView alloc] initWithTitle:@"Profile Picture" message:@"Set as profile picture?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alt.tag=tag;
        [alt show];
        return;
    }
//    
    UserMedia *um = [[DBHelper sharedObject] getObjectsforEntity:@"UserMedia" ShortBy:nil isAscending:YES predicate:[NSPredicate predicateWithFormat:@"tag == %i",tag]][0];
    
    [self itemDidFinishPlaying:nil];
    self.theMoviPlayer.contentURL= [NSURL URLWithString:um.url];
    self.theMoviPlayer.scalingMode = MPMovieScalingModeFill;
    self.theMoviPlayer.view.frame = btn.frame;
    [self.view addSubview:self.theMoviPlayer.view];
    [self.theMoviPlayer play];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    self.theMoviPlayer.controlStyle = MPMovieControlStyleNone;
//
    
    
}

#pragma mark -
#pragma mark - UIAlertView delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0) {
        return;
    }
    
    UserMedia *ui=nil;
    for (int i=0; i<[self.arrImages count]; i++) {
        UserMedia *u=[self.arrImages objectAtIndex:i];
        if ([u.tag intValue]==alertView.tag) {
            ui=u;
        }
    }
    
    if (ui==nil) {
        return;
    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading..."];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:[User currentUser].fbid forKey:PARAM_ENT_USER_FBID];
    [dictParam setObject:ui.media_id forKey:PARAM_ENT_NEW_IMAGE_ID];
    [dictParam setObject:ui.tag forKey:PARAM_ENT_NEW_PRF_INDEX_ID];
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromPath:METHOD_UPDATE_PROFILE_PIC withParamData:dictParam withBlock:^(id response, NSError *error) {
        if (response) {
            if ([[response objectForKey:@"errFlag"] intValue]==0) {

                NSArray *arr = [response objectForKey:@"Userphotos"];
                for (NSDictionary *dict in arr) {

                    UserMedia *um=[[DBHelper sharedObject] getObjectsforEntity:@"UserMedia" ShortBy:nil isAscending:YES predicate:[NSPredicate predicateWithFormat:@"media_id == %i",[[dict objectForKey:@"image_id"] integerValue]]][0];
//                    um.media_id = [dict objectForKey:@"image_id"];
                    um.url = [dict objectForKey:@"image_url"];
                    um.tag = @([[dict objectForKey:@"index_id"] integerValue]);
                    [[DBHelper sharedObject] saveContext];
                }
                
                if([[UserDefaultHelper sharedObject] facebookLoginRequest]!=nil) {
                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] initWithDictionary:[[UserDefaultHelper sharedObject] facebookLoginRequest]];
                    UserMedia *uiPP = nil;
                    for (int i=0; i < [self.arrImages count]; i++) {
                        UserMedia *u = [self.arrImages objectAtIndex:i];
                        if ([u.tag intValue]==0) {
                            uiPP=u;
                        }
                    }
                    if (uiPP != nil) {
                        [dictParam setObject:uiPP.url forKey:PARAM_ENT_PROFILE_PIC];
                        [[UserDefaultHelper sharedObject] setFacebookLoginRequest:dictParam];
                    }
                    [User currentUser].profile_pic = [dictParam objectForKey:PARAM_ENT_PROFILE_PIC];
                }
                [self reloadImagesFromID:0 toID:6 withCallBack:nil];
            }
        }
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }];
}

#pragma mark -
#pragma mark - UIActionSheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 20) {
        switch (buttonIndex)
        {
            case 0:
                [self openCamera];
                break;
            case 1:
                [self chooseFromLibaray];
                break;
            case 2:
                break;
        }
    } else if (actionSheet.tag == 30) {
        switch (buttonIndex)
        {
            case 0:
                [self openVideoCamera];
                break;
            case 1:
                [self chooseVideoFromLibrary];
                break;
            case 2:
                break;
        }
    }
}

- (void)openVideoCamera {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *videoRecorder = [[UIImagePickerController alloc]init];
        NSArray *sourceTypes = [UIImagePickerController availableMediaTypesForSourceType:videoRecorder.sourceType];
        NSLog(@"Available types for source as camera = %@", sourceTypes);
        if (![sourceTypes containsObject:(NSString*)kUTTypeMovie] ) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Device Not Supported for video Recording."                                                                       delegate:self
                                                  cancelButtonTitle:@"Yes"
                                                  otherButtonTitles:@"No",nil];
            [alert show];

            return;
        }
        videoRecorder.sourceType = UIImagePickerControllerSourceTypeCamera;
        videoRecorder.mediaTypes = [NSArray arrayWithObject:(NSString*)kUTTypeMovie];
        videoRecorder.videoQuality = UIImagePickerControllerQualityTypeLow;
        videoRecorder.videoMaximumDuration = 8;
        videoRecorder.delegate = self;
//        self.recorder_ = videoRecorder;

        [self presentViewController:videoRecorder animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Device Not Supported for video Recording."                                                                       delegate:self
                                              cancelButtonTitle:@"Yes"
                                              otherButtonTitles:@"No",nil];
        [alert show];
    }
}

- (void)chooseVideoFromLibrary {
   
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.view.tag=selectedBtnTag;
    imagePickerController.delegate = self;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePickerController.editing=NO;
    imagePickerController.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, nil];
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}

-(void)openCamera
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.view.tag=selectedBtnTag;
        imagePickerController.delegate = self;
        imagePickerController.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePickerController.editing=YES;
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }
    else{
        UIAlertView *alt=[[UIAlertView alloc]initWithTitle:@"" message:@"Camera Not Available" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alt show];
    }
}

-(void)chooseFromLibaray
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.view.tag=selectedBtnTag;
    imagePickerController.delegate = self;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.editing=YES;
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}

#pragma mark -
#pragma mark - UIImagePickerController Delegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
//    selectedBtnTag = 0;
//    [self deleteImage];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Uploading..."];
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
    
        UIImage *img = [[UtilityClass sharedObject] scaleAndRotateImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
        
        NSMutableDictionary *dictParam = [NSMutableDictionary new];
        [dictParam setObject:[User currentUser].fbid forKey:PARAM_ENT_USER_FBID];
        [dictParam setObject:[NSString stringWithFormat:@"%d",picker.view.tag] forKey:PARAM_ENT_INDEX_ID];
        
        NSData *imageToUpload = UIImageJPEGRepresentation(img, 1.0);
        if (imageToUpload) {
            NSString *strImage=[Base64 encode:imageToUpload];
            if (strImage) {
                [dictParam setObject:strImage forKey:PARAM_ENT_USERIMAGE];
            }
        }
        
        __weak typeof(self) weekSelf_ = self;
        AFNHelper *afn=[AFNHelper new];
        [afn getDataFromPath:METHOD_UPLOAD_USER_IMAGE withParamData:dictParam withBlock:^(id response, NSError *error) {
            if (response) {
                if ([[response objectForKey:@"errFlag"] intValue]==0) {
                    UserMedia *um = [[DBHelper sharedObject] createObjectForEntity:@"UserMedia"];
                    um.tag = @(selectedBtnTag);
                    um.media_id = @([[response objectForKey:@"ent_image_id"] integerValue]);
                    um.url = [response objectForKey:@"picURL"];
                    um.thumb_url = nil;
                    [[DBHelper sharedObject] saveContext];
                    NSLog(@"iu: %@",um);
                    
                    [weekSelf_ reloadImagesFromID:0 toID:6 withCallBack:^{
                        [[ProgressIndicator sharedInstance] hideProgressIndicator];
                    }];
                }
            }
            ;
        }];
        
    } else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
        
        NSURL * mediaURL = [info objectForKey:UIImagePickerControllerMediaURL];
        AVAsset *video = [AVAsset assetWithURL:mediaURL];
        AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:video presetName:AVAssetExportPresetMediumQuality];
        exportSession.shouldOptimizeForNetworkUse = YES;
        exportSession.outputFileType = AVFileTypeMPEG4;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        basePath = [basePath stringByAppendingPathComponent:@"videos"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:basePath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:basePath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        NSURL *compressedVideoUrl=nil;
        compressedVideoUrl = [NSURL fileURLWithPath:basePath];
        long CurrentTime = [[NSDate date] timeIntervalSince1970];
        NSString *strImageName = [NSString stringWithFormat:@"%ld",CurrentTime];
        compressedVideoUrl = [compressedVideoUrl URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",strImageName]];
        
        exportSession.outputURL = compressedVideoUrl;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            if (exportSession.status == AVAssetExportSessionStatusCompleted) {

                [self upload:[NSData dataWithContentsOfFile:compressedVideoUrl.path] path:compressedVideoUrl.path];
            }
        }];
    }
}

- (void)upload:(NSData *)data path:(NSString *)path{
        
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:API_URL]];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"upload_user_video" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFormData:[[User currentUser].fbid dataUsingEncoding:NSUTF8StringEncoding] name:PARAM_ENT_USER_FBID];
        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%i",selectedBtnTag] dataUsingEncoding:NSUTF8StringEncoding] name:PARAM_ENT_INDEX_ID];
        [formData appendPartWithFileData:data
                                    name:@"ent_uservideo"
                                fileName:@"video"
                                mimeType:@"video/mp4"];
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *theOperation, id responseObject) {
        NSLog(@"Success: headers:%@, \n\n\nresponse:%@",theOperation.response.allHeaderFields, theOperation.responseString);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:theOperation.responseData options:NSJSONReadingMutableContainers error:nil];
        UserMedia *um = [[DBHelper sharedObject] createObjectForEntity:@"UserMedia"];
        um.media_id = @([[dict objectForKey:@"ent_image_id"] integerValue]);
        um.url=[dict objectForKey:@"vidURL"];
        um.tag=@(selectedBtnTag);
        
        um.thumb_url = [um.url hasSuffix:@"mp4"] ? um.thumb_url = [NSString stringWithFormat:@"http://vtinder.com/iOS/php/php/pics/%@",dict[@"thumb_url"]] : nil;
        [[DBHelper sharedObject] saveContext];
        
        [self reloadImagesFromID:0 toID:6 withCallBack:^{
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
        }];
        
    } failure:^(AFHTTPRequestOperation *theOperation, NSError *error) {
        NSLog(@"Error:%@ ,%@,%@",error,theOperation.response.allHeaderFields,theOperation.responseString);
    }];
    [operation start];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect rect=self.view.frame;
    if (IS_IPHONE_5)
    {
        rect.origin.y=-110;
    }
    else
    {
        rect.origin.y=-170;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame=rect;
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect rect=self.view.frame;
    rect.origin.y=0;
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame=rect;
    }];
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
