//
//  EditProfileVC.h
//  Tinder
//
//  Created by Elluminati - macbook on 14/05/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>
#import "UserMedia.h"
#import "VideoButton.h"

@interface EditProfileVC : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate>
{
    int selectedBtnTag;
    
}
@property(nonatomic,copy)NSString *strStatus;
@property(nonatomic,weak)IBOutlet UITextField *txtStatus;
@property (nonatomic, copy) NSString *mp4VideoPath;
@property (nonatomic, strong) NSData *audioVideoData;

-(IBAction)onClickChangeStatus:(id)sender;
-(IBAction)onClickBtn:(id)sender;
-(IBAction)onClickImage:(id)sender;

@end
